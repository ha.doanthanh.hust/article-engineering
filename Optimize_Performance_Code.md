# Các cách tối ưu performance code trong NodeJS

## Giới thiệu

Làm việc với NodeJS, chắc hẳn Loop và Promise là 2 thứ mà các Dev như chúng ta rất hay gặp. Viết code để chạy các Loop và Promise thì rất dễ. Nhưng đã bao giờ bạn phải đau đầu vì vấn đề tối ưu Loop và Promise, hoặc cả Loop và Promise lồng nhau trong NodeJS chưa ? Hoặc làm thế nào để có thể tối ưu được performance của từng đoạn code, mà vẫn giữ code được clean. Chúng ta cùng đào sâu vào vấn đề này trong bài viết dưới đây.

## 1. Loop là gì ?

Loop là gì chắc hẳn các lập trình viên đều biết. Có rất nhiều loại Loop. Ví dụ `For Loop, While Loop, For Of, For In, forEach` hoặc 1 số hàm chuyên dùng cho Array như: `Map, Reduce, Filter, Some, ...` Và cách dùng thì cũng rất đơn giản và basic. Nên mình sẽ bỏ qua và tập trung vào phần tối ưu performance trong Loop

Ví dụ 1 bài toán trên Leetcode: [Link](https://leetcode.com/problems/container-with-most-water/)

```markdown
Given n non-negative integers a1, a2, ..., an,
where each represents a point at coordinate (i, ai). n vertical lines are drawn
such that the two endpoints of the line i is at (i, ai) and (i, 0).
Find two lines, which, together with the x-axis forms a container, such that the container contains the most water.

Notice that you may not slant the container.
```

Example 1:
![question](./images/question_11.jpg)

```markdown
Input: height = [1,8,6,2,5,4,8,3,7]
Output: 49
Explanation: The above vertical lines are represented by array [1,8,6,2,5,4,8,3,7].
In this case, the max area of water (blue section) the container can contain is 49.
```

Nôm na là tính diện tích lớn nhất của nước được tạo bởi các cột

1 cách giải đơn giản đó là sử dụng 2 vòng For Loop. Sau đó tính width của Area = j - i.

```typescript
width = j - i;
```

Sau đó tính height hiện tại theo từng cột nhỏ hơn (so sánh height[i] và height[j]):

```typescript
current_height = height[i] <= height[j] ? height[i] : height[j];
```

Sau đó tính diện tích của water area theo width và height:

```typescript
area = width * current_height
```

Sau đó tìm kết quả (result) dựa trên các kết quả tìm được.

```typescript
const maxArea = (height) => {
  const n = height.length;
  let result = 0;
  for (i = 0; i < n; i++) {
    for (j = i + 1; j < n; j++) {
      let width = j - i;
      let current_height = height[i] <= height[j] ? height[i] : height[j];
      let area = width * current_height;
      if (area > result) {
        result = area;
      }
    }
  }
  return result;
};
```

Cách này chắc chắn sẽ cho 1 kết quả đúng. Nhưng hãy nhìn lại bài toán. Với cách này, chúng ta có 2 loop, mỗi loop sẽ lặp từ 0 đến length. Với 2 loop. Số lần cần tính toán sẽ vào khoảng n^2 `(O(n^2))` lần. Khi `n = 1000`. Chúng ta sẽ cần phải tính `n^2 = 1.000.000` phép tính để cho ra kêt quả tương ứng. Vậy sẽ ra sao nếu `n = 10.000 hoặc 100.000` ? Chắc hẳn sẽ gặp phải lỗi Max Execution Time. Và độ phức tạp của thuật toán này là `O(n^2)`.

Và cách làm này không hề tính đến performance của thuật toán. Hãy thử nhìn vào cách 2 dưới đây:

```typescript
const maxArea = (height) => {
  let left = 0;
  let right = height.length - 1;
  let result = 0;
  while (left < right) {
    let height = Math.min(height[left], height[right]);
    let width = r - l;
    result = Math.max(result, height * width);
    if (height[left] < height[right]) {
      l++;
    } else {
      r--;
    }
  }
  return result;
};
```

Hãy thử phân tích cách 2. Chúng ta sẽ dùng while loop. Condition là `l < r`. Vẫn sẽ tính width và height. Sau đó tính result dựa trên Math.max. Sau mỗi vòng lặp, so sánh giá trị của `height[l] và height[r]`. Nếu `height[l] > height[r]`, sẽ giảm right và ngược lại. Hãy thử tính toán độ phức tạp của thuật toán. Chúng ta chỉ cần tính toán khoảng `n - 1 lần (O(n))`. Tức là với `n = 1000` sẽ chỉ cần thực hiện `1000 phép tính`, `n = 10.000` sẽ chỉ cần `10.000 phép tính (giảm 10.000 lần so với O(n^2))`. Nôm na có thể nói là sẽ nhanh gấp n lần so với cách 1. Thời gian execute giảm đáng kể phải không nào. Chỉ cần thay đổi cách suy nghĩ, thuật toán của bạn nhanh và được tối ưu đáng kể rồi đó

## 2. Vậy nên, độ phức tạp của thuật toán là gì ? Và ảnh hưởng của nó đến Performance

Performance thường được tính toán dựa trên 2 tiêu chí: Tốc độ xử lý `(Time complexity)` và bộ nhớ cần sử dụng `(Space complexity)`. Nhưng tuỳ theo phần cứng, server, host. Chúng ta có rất nhiều bộ nhớ nên Space thường bị bỏ qua. Và chúng ta sẽ chỉ đào sâu về Time complexity (Big O)

| Big O      | Rank                               | Ý nghĩa                                                   |
| ---------- | ---------------------------------- | --------------------------------------------------------- |
| O(1)       | 😎 - Excellent                     | Tốc độ không phụ thuộc vào độ lớn data                    |
| O(log n)   | 😁 - Good                          | Nếu data tăng gấp 10 lần thì tốn gấp 2 thời gian          |
| O(n)       | 😕 - Fair                          | Nếu dữ liệu tăng gấp 10 lần thì tốn gấp 10 lần thời gian  |
| O(n log n) | 😖 - Bad                           | Nếu dữ liệu tăng gấp 10 lần thì tốn gấp 20 lần thời gian  |
| O(n^2)     | 😫 - Horrible                      | Nếu dữ liệu tăng gấp 10 lần thì tốn gấp 100 lần thời gian |
| O(2^n)     | 😱 - God: Fuck you                 | Dữ liệu tăng gấp 2 thôi là toang rồi đấy bạn à            |
| O(n!)      | 🤬 - God Father: you, son of bitch | कहने के लिए बोरिंग                                        |

![big-o](./images/big-o.png)

### O(1)

Khi ta nói một thuật toán nào đó có độ phức tạp `O(1)` có nghĩa là tốc độ xử lý không phụ thuộc vào độ lớn của bộ dữ liệu. Tìm kiếm kết quả của một bài toán trên bộ dữ liệu đều có tốc độ như nhau, giống như được đọc trực tiếp từ ô nhớ trên RAM

Ví dụ 1:

```typescript
const s = (n * (n - 1)) / 2;
```

### O(n)

Thuật toán được sử dụng trong hàm Array `(find, map, filter, for loop...)` có độ phức tạp `O(n)`. Điều này có nghĩa là thời gian xử lý của thuật toán Array phụ thuộc tuyến tính vào số phần tử trong array. Việc tìm kiếm trong một array với 100 phần tử có thể tốn thời gian gấp 100 lần so với array một phần tử.

Phần lớn các đoạn code sử dụng vòng lặp sẽ có độ phức tạp O(n).

Ví dụ 2:

```typescript
const a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const b = a.map((item) => item * 2); // [2, 4, 6, 8, 10, 12, 14, 16, 18, 20];
```

### O(n^2)

Các đoạn code có độ phức tạp `O(n^2)` thường là các vòng lặp lồng nhau. Điều này rất dễ hiểu và có thể tính toán được: với mỗi vòng lặp có độ phức tạp `O(n)`, vậy với một vòng lặp thứ hai được thực hiện trong mỗi vòng lặp trên ta có độ phức tạp là `O(n^2)`. Vậy nên, nếu ta có 5 vòng lặp lồng nhau, độ phức tạp ta có là `O(n^5)`.

Ví dụ 3:

```typescript
let s = 0; // O(1)
for (let i = 0; i <= n; i++) {
  let p = 1; // O(1)
  for (let j = 1; j <= i; j++) {
    p = (p * i) / j; // O(1)
  }
  s = s + p; // O(1)
}
```

Độ phức tạp của thuật toán này là: `O(1) + O(1) + O(n * (n - 1) / 2) + O(1) + O(1) = O(n^2)`
Fun fact: Các phép cộng trừ thông thường là vô nghĩa trong lý thuyết tính toán, vậy nên ta hoàn toàn có thể viết rằng `O(2n^3 - 4n^2 + 1) = O(n^3)`

### O(nlogn)

Các thuật toán `O(nlogn)` thường là kết quả của một thuật toán tối ưu do một nhà toán học nào đó nghĩ ra để cải thiện tốc độ của một bài toán mà cách thông thường (vét cạn) phải mất thời gian cỡ `O(n^2)`.

Thường thì khó mà có thể chỉ nhìn qua là biết được một đoạn code có phải là `O(nlogn)` hay không mà đòi hỏi phải tính toán và chứng minh chi tiết.

Dù vậy, điều quan trọng cần phải ghi nhớ là các thuật toán tìm kiếm nổi tiếng thường có kết quả là `O(n log n)`. Hàm Ruby Array#sort sử dụng một biến thể của thuật toán quicksort, có độ phức tạp cận trên là `O(n log n)` và trung bình là `Θ(n^2)`

### O(2^n)

Các thuật toán `O(2^n)` thường là các thuật toán dạng đệ quy. Ta có thể thấy thuật toán đệ quy nổi tiếng là tính số `Fibonacci number`.

Ví dụ:

```typescript
const fibonacci = (number) => {
  if (number <= 1) {
    return number;
  }

  return fibonacci(number - 2) + fibonacci(number - 1);
};
```

Với `number = 3`, thì `fibonacci(3) = 2`. Tương tự với number = 10 thì `fibonacci(10) = 55`

### O(n!)

Các thuật toán `O(n!)` thường là thuật toán dạng đệ quy trong vòng lặp. Thường thì không nên hoặc rất ít khi cần vận dụng đến O(n!). Và lỗi này thường gặp ở các dev mới học về đệ quy và thích vận dụng đệ quy bừa bãi.

Hoặc 1 ví dụ nổi tiếng của thuật toán `O(n!)` là `Heap Algorithm`.

Ví dụ: cho 1 array các số #, viết thuật toán để tạo ra tất cả các hoán vị của array đó

```typescript
const array = [1, 2, 3];

const permutation = (n, arr) => {
  if (n === 1) {
    console.log(arr);
    return;
  }

  for (let i = 0; i < n; i++) {
    permutation(n - 1, arr);
    if (n % 2 === 0) {
      swap(arr, i, n - 1);
    } else {
      swap(arr, 0, n - 1);
    }
  }
};

const swap = (arr, indexA, indexB) => {
  let temp = arr[indexA];
  arr[indexA] = arr[indexB];
  arr[indexB] = temp;
};

// Execute permutation
permutation(array.length, array);
```

Kết quả sẽ là:

```
[1, 2, 3]
[2, 1, 3]
[3, 1, 2]
[1, 3, 2]
[2, 3, 1]
[3, 2, 1]
```

Ta có thể thấy với mỗi vòng lặp, ta sẽ call lại `function permutation()` 1 lần với độ dài array giảm đi 1.
Vì vậy độ phức tạp của thuật toán này sẽ là `O(n!)`

Đó là lý do độ đôi khi anh em cần để ý đến hướng giải quyết vấn đề và cách làm. Chứ không chỉ làm để cho code chạy được.

Bonus Recommendation: 1 số các trang web về thuật toán nên học. Nên làm mỗi ngày or mỗi tuấn 1 vài bài. Vì nó thực sự tốt cho tư duy

1. [Leetcode](https://leetcode.com)
2. [Hackerrank](https://www.hackerrank.com/)
3. [Codewars](https://www.codewars.com/)

## 3. Promise là gì và cách tối ưu trong Promise

Ai cũng từng 1 lần cho bạn mượn tiền, có đứa hứa sẽ trả (_pending_), có đứa trả (_fulfilled_), và cũng có đứa một đi không trở lại (_reject_).

_Đây cũng 3 trạng thái của Promise._

Lúc còn là tờ giấy nợ, pending.
Lúc dc trả tiền, `Promise` được `resolve`, đây là trạng thái `fulfilled` trong truyền thuyết. Chúng ta cũng có kết quả trả về là số tiền cầm trên tay, ôi những thứ tưởng đã mất khi quay trở về mới thật vi diệu làm sao, bố mày thề ko bao giờ cho mượn tiền nữa.
Lúc bị quịt, `Promise` bị `reject`, đây là trạng thái cuối cùng trước khi chúng ta lao ra quán nhậu để chia tay 1 cuộc tình. Có thể là tình yêu, hoặc là tình bạn.

### Khi nào dùng Promise.

Khi 1 function đang kẹt nhưng hứa sẽ trả tiền cho chúng ta vào 1 tương lai xa. Nhưng chúng ta lại cần dùng số tiền đó trong 1 function khác. Khi đấy Promise sẽ đóng vai trò là giá trị chúng ta cần, để chúng ta có thể gán nợ ngay, mà ko cần đợi thằng kia trả tiền.

Lúc này anh em cứ nghĩ Promise là 1 tờ giấy lộn, nhưng có tác dụng thật, là minh chứng (proxy) cho cục tiền nợ có nguy cơ mất 99% kia.

```typescript
const traTienEmAnhOi = () => {
  return new Promise((resolve, reject) => {
    // tâm sinh lý ngẫu nhiên
    const isHappy = Math.random() >= 0.5;

    // nếu vui thì gọi resolve để trả tiền
    if (isHappy) {
      const money = 1000000;
      return resolve(money); //  Promise dc fulfilled
    }

    // không vui quịt luôn
    // nhớ cho ng ta biết lý do vì chúng ta là lập trình viên lịch sự.
    const relief_reason = "Để yên t còn trả. Đòi nhiều đéo trả nữa!";
    reject(reason); //  Promise ở trạng thái reject
  });
};
```

Có 2 cách để thực thi hành động kêu gọi đòi tiền kia.

- 1 là sử dụng Chaining `(.then và .catch)`
- 2 là sử dụng `async và await` (Recommend anh em nên dùng cách này vì code sẽ clean hơn rất nhiều)

```typescript
// Thực thi hành động kêu gọi trả tiền
// Cách 1
traTienEmAnhOi() // trả về 1 Promise.
  .then((money) => {
    // chúng ta có thể làm gì đó với số tiền này ngay lập tức
    // ơ làm gì đây
    return party(money);
  })
  .catch((relief_reason) => {
    console.log(relief_reason);
  })(
  // Cách 2 -- Viết tạm cái IIFE function thì mới có async =)))
  async () => {
    try {
      await traTienEmAnhOi();
    } catch (relief_reason) {
      console.log(relief_reason);
    }
  }
);
```

### Các vấn đề thường gặp

Ví dụ ta có 1 func `veNha`, chỉ chạy/execute khi đã `anNhau, uongRuou, hutBong`. Các bước tiến hành thông thường sẽ là như này

```typescript
async () => {
  await anNhau(); // 2 tiếng
  await uongRuou(); // 1 tiếng
  await hutBong(); // 1 tiếng
};
```

Hãy để ý nếu `anNhau, uongRuou và hutBong` được thực hiện lần lượt, tổng thời gian để hoàn thành `(time execution)` 3 việc này là tầm 4 giờ. Trong khi đấy vợ chỉ cho đi chơi tầm 2 tiếng thì phải làm thế nào 😓😓 😱😱.

Mà thực ra thì `anNhau, uongRuou và hutBong` đều có thể tiến hành cùng 1 lúc (nhanh chết, nhanh phê, không ăn thì sao có sức mà uống rượu + hút bóng) và cùng chả ai quan tâm việc hút bóng trước hay uống rượu hay ăn trước. Vậy tại sao không thực hiện cả 3 hành động 1 lúc cho nhanh phê nhỉ 😏😏, lại còn có thể về đúng giờ với vợ con nữa chứ 🤫🤫.

```typescript
async () => {
  await Promise.all([anNhau(), uongRuou(), hutBong()]); // khoảng 2 tiếng
};
```

`Promise.all` sẽ thực hiện các hành động cùng 1 lúc `(parallel)`. `anNhau, uongRuou, hutBong` ai xong trước thì xong. nhưng vẫn phải đợi ăn xong, uống xong, hút bóng xong r mới về được phải không nào. Nên thông thường sẽ mất khoảng 2 tiếng (bằng với thời gian `anNhau`).

![promise_parallel](./images/promise-time-execution.png)

### Nhưng thực tế sẽ phụ thuộc vào khả năng sinh lý của từng người 😘😘

```
Note: Promise.all won't actually run the requests exactly in parallel
and the performance in general will greatly depend on the exact environment
in which the code is running and the state of it (for instance the event loop)
but this is a good approximation.
```

Hãy để ý đến thuật ngữ `Space complexity` đã được đề cập đến ở trên. Thằng nào khoẻ, vừa ăn vừa uống vừa hút bóng được thì sẽ xong trong 2 tiếng. Nhưng mấy ông _ysl_, uống 1 chén nghỉ 15' thì làm sao có thể vừa uống, vừa ăn, vừa hút bóng được đúng không.

### Mỗi người như 1 cái Server. Cơ quan nội tạng, ý thức bên trong như 1 cái CPU/RAM 🧐🧐

- Ăn ngốn 20% RAM
- Uống rượu nhiều, phê phê thì ngốn tiếp 40% RAM
- Hút bóng phê sml thì chắc tầm 30% RAM

Cộng tổng lại thì chắc cũng khoảng 90%. Nhưng thực tế thì không ai cộng như vậy đâu. Nôm na khi thực hiện từng hành động riêng biệt, cơ thể của chúng ta hoàn toàn có thể xử lý riêng từng cái. Đợi đỡ no rồi mới uống rượu, đợi đỡ say rồi mới hút bóng. Thì mức hoạt động của các cơ quan chỉ tầm khoảng 40 > 50%. Hoàn toàn tỉnh táo để về với vợ con 🥺🥺

Ví dụ bạn không ăn nhậu 1 mình, mà ăn nhậu theo team tầm 100 thằng:

```typescript
const teamAnNhau = Array.from(Array(100).keys()); // Thế là có 100 đứa rồi
async () => {
  const anNhauTogether = teamAnNhau.map(async (person) => {
    const zdo = await Promise.all([anNhau(), uongRuou(), hutBong()]); // 2 tiếng /1 người
    return zdo;
  });
  return await Promise.all(anNhauTogether); // Có phải vẫn là 2 tiếng không ?
};
```

Phải chăng bạn đang nghĩ 100 thằng cùng nhậu 1 lúc thì vẫn là 2 tiếng phải không. Thực ra ở ngoài đời thì nó đúng là như thế. Vì 100 thằng như 100 cái Server khác nhau.

Đổi ngược lại là 1 mình bạn nhậu cùng lúc với 100 bàn thì sao ?

Nhưng nếu thực hiện cùng 1 lúc mà không đủ khoẻ thì biết tay nhau r đấy. Có khi đang uống rượu xong combo thêm cái hút bóng, phê quá 😨😨, xong kèm theo việc gọi tên chị "Huệ" 🤮🤮 trong đêm nữa thì _tỉ lệ đạt ngưỡng cảnh báo (> 75% -> 80%)_ là điều quá bình thường. Đôi khi còn _sập_ tại chỗ nữa ấy chứ :))).

Và 1 khi đã sập thì toang hết chứ còn gì nữa. làm sau mà ăn nhậu, hút bóng hay uống rượu được nữa đúng không. Nó sẽ chạy trơn tru khi bạn đảm bảo được sức khoẻ của bạn đủ để 100 thằng nó thông 🥴🥴

Nhưng cứ yên tâm. Nếu bạn khoẻ, uống rượu như uống nước lã, hay thích chơi bóng bay từ bé thì cứ yên tâm. Sau 2 tiếng bạn vẫn tỉnh táo, khoẻ mạnh để về làm shot nữa với vợ, ny đc thôi 🤤🤤

```
Rút ra bài học: Nên biết hành động nào có thể đồng thời thực hiện,
hành động nào nên thực hiện lần lượt để tăng tốc độ xử lý (performance)
nhưng cũng không làm ảnh hưởng quá nhiều đến RAM/CPU
```

Nhưng Promise.all chỉ trả về kết quả khi cả 100 thằng còn sống sót quay về. 1 con ngựa đau cả tàu bỏ cỏ. 1 thằng xịt vào viện thì 99 thằng còn lại cũng phải vào viện thăm nó chứ còn gì nữa 😅😅

Mà nhậu 100 thằng cùng 1 quán thì hơi căng. Lại còn ngồi cùng bàn nữa. Vậy làm thế nào để có thể cho 100 thằng được ngồi bàn đó, ăn quán đó. nhưng 1 bàn chỉ giới hạn (limit) là 10 thằng. Thằng nào xong trước thì cho về trước. Chứ bắt nó ở lại ngồi nhìn 99 thằng còn lại thì cũng tội em nó ra 🤔🤔

### Làm thế nào để giới hạn số người cùng nhậu 1 lúc mà vẫn đảm bảo được tính linh hoạt ?

Tính linh hoạt ở đây là thằng nào xong sớm thì về sớm đó. Chứ ở lại đợi nhau có mà toang à.

```typescript
import * as _ from "lodash";

const teamAnNhau = Array.from(Array(100).keys());
// Thường cái zdo này sẽ là dạng Long Request như việc nhậu. mất đến hơn 2 tiếng 1 thằng
const zdo = async (person) => {
  await Promise.all([anNhau(person), uongRuou(person), hutBong(person)]);
};

const lachLuatIsEasy = async (limit) => {
  const banAn = [];

  do {
    if (teamAnNhau.length) {
      for (person of teamAnNhau) {
        const nhau = zdo(person).then(() => {
          // Khi đã xếp 1 thằng vào bàn thì đá nó ra khỏi teamAnNhau
          // để biết đc còn bao nhiêu thằng chưa đc xếp chỗ
          _.remove(teamAnNhau, (i) => i === person);
        });
        banAn.push(nhau);
      }

      // Khi bàn ăn đã xếp đủ hoặc hơn số giới hạn mà chủ quán đề ra
      // thì check xem thằng nào ăn xong rồi đá nó ra để còn xếp thằng khác vào
      if (banAn.length >= limit) {
        await Promise.race(banAn);
      }
    }
  } while (batch.length);

  // Xếp xong mà vẫn còn thừa số thằng nhỏ hơn limit thì xếp nốt ỏ đây =))
  return Promise.all(banAn);
};
// Ăn nhậu thông minh
lachLuatIsEasy(5);
```

Với cách này, ta có thể đảm bảo rằng cứ khi nào xếp đủ hoặc lớn hơn limit của chủ quán thì mới đc nhậu. Mà thằng nào nhậu xong trước thì sẽ được về trước. Bởi vì cơ chế của `Promise.race` sẽ trả về thằng nào chạy nhanh nhất. Hoặc có việc phải về trước. Và chỉ trả về duy nhất 1 thằng. Nên có thể đảm bảo được mỗi lần chạy `Promise.race` sẽ chỉ trả về 1 giá trị resolve

Vậy nếu bạn thấy việc đang nhậu mà cứ có thằng ra mới có thằng vào nó rách việc, lại còn lâu.

### Bạn muốn đổi thành cứ 5 thằng vào 1 lần. Xong thì xong hết. còn không thì không cho nó về thì sao ?

Cái này thì còn _đơn giản_ hơn ☺️☺️. Chỉ cần chia thành nhóm 5 người. Cứ 5 người này xong, 5 người khác sẽ vào thay thế. Giảm tải cho quán. Mà không sợ đông quá rồi 1 thằng _ngỏm_ thì cả làng cùng vào viện thăm nó

```typescript
const teamAnNhau = Array.from(Array(100).keys());
// Thường cái zdo này sẽ là dạng Long Request như việc nhậu. mất đến hơn 2 tiếng 1 thằng
const zdo = async (person) => {
  await Promise.all([anNhau(person), uongRuou(person), hutBong(person)]);
};

const lachLuatIsEasyVersion2 = async (limit) => {
  let results = [];
  const chiaBan = Math.ceil(teamAnNhau / limit);

  // Running Promises in parallel in batches
  for (let i = 0; i < chiaBan; i++) {
    const batchStart = i * limit;
    const groupPerson = teamAnNhau.slice(batchStart, batchStart + limit);
    const batchPromises = groupPerson.map((person) => zdo(person));
    // Harvesting
    const batchResults = await Promise.all(batchPromises);
    results = results.concat(batchResults);
  }
  return results;
};

lachLuatIsEasyVersion2(5);
```

Cách này sẽ chậm hơn so với cho 100 thằng nhậu cùng 1 bàn. Nhưng đôi khi cũng có thể nhanh hơn nếu số ng ăn nhậu không quá lớn. Còn sức chứa của quán thì có hạn

Nghĩ thì được nhiều rồi đấy

### Nhưng chẳng may khi đến quán, thằng chủ quán bắt xác minh danh tính từng người, độ tuổi rồi mới cho vào nhậu thì sao ?

Thông thường, chúng ta sẽ phải check lần lượt `Chứng minh thư (Identification)` của từng thằng như sau:

```typescript
const teamAnNhau = Array.from(Array(100).keys()); // Thế là có 100 đứa rồi
const zdo = async (person) =>
  await Promise.all([anNhau(person), uongRuou(person), hutBong(person)]);
async () => {
  for (const person of teamAnNhau) {
    // Lục trên db xem chứng minh thư của từng thằng
    const tren18Tuoi = await soYeuLyLich.findOne({
      _id: person,
      age: { $lte: 18 },
    });
    // Không trên 18 tuổi thì phắn. Trẻ con tí tuổi đầu nhậu với chả nhẹt
    if (!tren18Tuoi) {
      continue;
    }
    // Pass rồi thì nhậu thôi
    await zdo(person);
  }
};
```

Ờ. Nhìn có vẻ ok đấy. chạy ngon ăn đấy. Nhưng khoan. Tại sao chủ quán lại cần phải bắt bọn nó xếp hàng rồi đếm từng thằng 1 xong kiểm tra nhỉ. Sao không lấy hết chứng minh thư của chúng nó ra rồi phân loại trong 1 lượt luôn cho nhanh có phải ok hơn không

```typescript
const teamAnNhau = Array.from(Array(100).keys()); // Thế là có 100 đứa rồi
const zdo = async (person) =>
  await Promise.all([anNhau(person), uongRuou(person), hutBong(person)]);

const teamAnNhauTren18Tuoi = await soYeuLyLich.find({
  _id: { $in: [teamAnNhau] },
  age: { $lte: 18 },
});
// Pass rồi thì đi ăn nhậu thôi
async () => {
  for (const person of teamAnNhauTren18Tuoi) {
    await zdo(person);
  }
};
```

Cách viết sẽ ngắn, rõ ràng và tối ưu hơn rất nhiều. So với việc query 100 lần đến DB chỉ để lấy thông tin riêng của từng người thì ta có thể lọc luôn trong 1 lần. Tương tự nếu như cần chứng minh thêm các thông tin gì trước khi nhậu, ta hoàn toàn có thể làm tương tự như trên. Và nó cũng gần giống với việc sử dụng `Memoization`

### Vậy Memoization là gì 🙄🙄. Nghe có vẻ cao siêu đấy

**Memoization** là một kỹ thuật tối ưu hóa, giúp tăng tốc các ứng dụng bằng cách lưu trữ kết quả của các lệnh gọi hàm (mà các hàm này được gọi là _Expensive function_) và trả về kết quả được lưu trong bộ nhớ cache khi có cùng một đầu vào yêu cầu (đã được thực thi ít nhất 1 lần trước đó rồi).

#### Expensive function là gì?

Đừng nhầm lẫn, chúng ta không có tiêu tiền ở đây. Trong bối cảnh của các chương trình máy tính, hai tài nguyên chính chúng ta có là thời gian và bộ nhớ. Do đó, một gọi hàm đắt tiền là gọi một hàm tiêu thụ khối lượng lớn của hai tài nguyên này trong khi thực hiện do tính toán nặng (code lởm hoặc yêu cầu phức tạp chẳng hạn).

Đối với điều này, _Memoization_ sử dụng bộ nhớ đệm để lưu trữ kết quả của các lần gọi hàm, để có thể truy cập nhanh chóng và dễ dàng sau này.

_Bộ đệm_ chỉ đơn giản là một kho lưu trữ dữ liệu tạm thời chứa dữ liệu, để dữ liệu đó có thể được phục vụ nhanh hơn cho các yêu cầu trong tương lai.

Thực ra có 1 ví dụ ngay trong bài này có thể sử dụng _Memoization_ đấy. Đoán xem là ví dụ nào 😋😋.

Chính là ví dụ về Fibonacci Number đấy.

```typescript
const fibonacci = (number) => {
  if (number <= 1) {
    return number;
  }

  return fibonacci(number - 2) + fibonacci(number - 1);
};
```

Nhìn sơ qua thì func tính fibonacci này ok đúng không. Cũng chả biết làm thế nào để tối ưu đc nó nữa đúng không =))). Thử nhìn đoạn code dưới đây xem sao nhé

```typescript
const fibonacciMemo = (number, memo) => {
  memo = memo || {};
  if (memo[number]) {
    return memo[number];
  }
  if (number <= 1) {
    return 1;
  }
  memo[number] = fibonacciMemo(number - 1, memo) + fibonacciMemo(number - 2, memo);
  return memo[number];
};
```

Nhìn kĩ lại sao loằng ngoằng thế nhỉ. Việc gì phải làm như thế nhỉ. Code vừa dài vừa khó hiểu.

But wait, hãy thử trải nghiệm khi tính `fibonacci(50)` xem nào =)). Đảm bảo là với cách thứ 1 thì đơ luôn browser đấy =))). Nhưng cách thứ 2 thì siêu nhanh luôn

Test thử 2 cách trên [jsbench](https://jsbench.me/) xem như nào nào.

![JSBench](./images/jsbench.png)

Với cách thứ 1, performance là `10918.49 ops/s ± 0.53%`. Với cách thứ 2, performance là `924708.55 ops/s ± 1.63%`

```
"ops/sec" stands for operations per second. That is how many times a test is projected to execute in a second.
Trans: Ops/sec là viết tắt của số Operations sẽ được thực hiện trong 1 giây. Đó là số lần bài kiểm tra sẽ được thực hiện trong 1 giây
```

Nôm na trong 1 giây, cách 1 sẽ thực hiện được khoảng `10900 lần`. Còn cách thứ 2 sẽ thực hiện được `924700 lần`. Tức là cách thứ 2 sẽ nhanh hơn cách thứ nhất khoảng `85 lần`. Quá kinh khủng :))). Với cách thứ 2 `fibonaciMemo`, ta hoàn toàn có thể tính đc số fibonacci thứ 1000 thoải mái mà không sợ đơ máy.

Thực ra vẫn còn có cách thứ 3 đấy. Và cách này thì quá kinh khủng rồi. Mặc dù code hơi lằng nhằng và khó hiểu ý. Nhưng vẫn clean à nha

```typescript
const fibonacci = (number) => {
  if (number <= 1) {
    return number;
  }

  return fibonacci(number - 2) + fibonacci(number - 1);
};

const memoizer = (fnc) => {
  let cache = {};
  return (number) => {
    if (cache[n] != undefined) {
      return cache[n];
    } else {
      let result = fnc(number);
      cache[n] = result;
      return result;
    }
  };
};

const fibonacciMemoizer = memoizer(fibonacci);
```

Có lẽ chỉ cần để đây và không cần nói nhiều

![JSBench2](./images/jsbench-2.png)

`206459115.21 ops/s ± 0.47%` 😱😱😱. Hơn `206 triệu ops/s` 🤤🤤🤤. Tức là nhanh hơn cách thứ 1 khoảng `18900 lần`, nhanh hơn cách thứ 2 khoảng `223 lần`.

### Vậy tại sao lại có sự chênh lệch nhiều đến như vậy ?

Nhắc lại về Memoization 1 chút nhé. Copy nguyên đoạn ở trên:

**Memoization** là một kỹ thuật tối ưu hóa, giúp tăng tốc các ứng dụng bằng cách lưu trữ kết quả của các lệnh gọi hàm (mà các hàm này được gọi là _Expensive function_) và trả về kết quả được lưu trong bộ nhớ cache khi có cùng một đầu vào yêu cầu (đã được thực thi ít nhất 1 lần trước đó rồi).

Cùng phân tích Code 1 tí nào:

```typescript
// Cách 1:
const fibonacci = (number) => {
  // Number nhỏ hơn 1 thì return ra number luôn
  if (number <= 1) {
    // 1 lần
    return number;
  }
  // Number lớn hơn 1 thì sẽ lấy tổng của 2 số trước nó.
  return fibonacci(number - 2) + fibonacci(number - 1);
};
```

Tức là với cách thứ 1, để tính được `fibonacci(5)`, máy tính sẽ phải thực hiện các phép tính sau đây: `fibonacci(5) = fibonacci(4) + fibonacci(3)`
Trong khi đó, `fibonacci(4) lại = fibonacci(3) + fibonacci(2)`
_... A few moment later_
Ta được kết quả: `fibonacci(5) = 8`. Tức là cộng 8 lần `fibonacci(1)` vào với nhau. Nhưng không có nghĩa là chỉ thực hiện 8 operations. Thực tế sẽ là:
1 ops cho `fibonacci(1) = 1` (1)
2 ops cho `fibonacci(2) = 1` (1 + 1)
4 ops cho `fibonacci(3) = 2` (1 + 1 + 2)
7 ops cho `fibonacci(4) = 3` (1 + 1 + 2 + 3)

Tổng func cần thực hiện sẽ rơi vào khoảng 1 + 2 + 4 + 7 = 14 lần. Và cách sẽ cấp số nhân lên rất nhiều lần nếu number càng lớn

Vậy tại sao với cách 2, ta lại có thể tối ưu được khoảng hơn 85 lần ?

```typescript
const fibonacciMemo = (number, memo) => {
  memo = memo || {};
  if (memo[number]) {
    return memo[number];
  }
  if (number <= 1) {
    return 1;
  }
  memo[number] = fibonacciMemo(number - 1, memo) + fibonacciMemo(number - 2, memo);
  return memo[number];
};
```

Trong đoạn mã trên, chúng ta điều chỉnh hàm để chấp nhận một tham số tùy chọn được gọi là _memo_ -- Đây chính là _bộ nhớ đệm_ nè. Chúng ta sử dụng `object memo` làm bộ nhớ `cache` để lưu trữ các số `Fibonacci` với các `value` tương ứng của chúng làm `key`, để có thể lấy bất cứ khi nào chúng được yêu cầu trong những lần tính toán kế tiếp.

Ở đây, chúng ta kiểm tra xem `memo` có được truyền vào hay không. Nếu có, chúng ta sẽ khởi tạo nó để sử dụng, nhưng nếu không, chúng ta sẽ đặt nó thành một object rỗng.
```typescript
memo = memo || {}
```

Tiếp theo, chúng ta kiểm tra xem có giá trị được lưu trong bộ nhớ cache cho khóa hiện tại n hay không? và trả về giá trị của nó nếu có. Như trong giải pháp trước đây, chúng ta chỉ định trường hợp kết thúc khi number nhỏ hơn hoặc bằng 1.
```typescript
if (memo[number]) {
  return memo[number]
}
```

Cuối cùng, chúng ta gọi đệ quy hàm với giá trị n nhỏ hơn, trong khi đó thì chuyển các giá trị được lưu trong bộ nhớ `cache (memo)` vào từng hàm, để sử dụng trong quá trình tính toán. Điều này đảm bảo rằng khi giá trị đã được tính toán trước đó và được lưu trong bộ nhớ cache, chúng ta không thực hiện tính toán như vậy lần thứ hai. Chúng ta chỉ đơn giản là lấy giá trị từ bộ nhớ cache chứ không cần phải execute lại các func. Chính điều này làm tăng tốc độ của thuật toán. Lưu ý rằng phải thêm kết quả cuối cùng vào bộ cache trước khi trả lại.

Việc execute lại 1 func luôn luôn tốn thời gian hơn việc cache hoặc tìm nó ở trong 1 *bộ nhớ đệm* nào đó

Vậy với cách thứ 3, tại sao lại nhanh hơn cách thứ 1 khoảng `18900 lần`, nhanh hơn cách thứ 2 khoảng `223 lần` ?

Ở cách thứ 2, chúng ta lưu cache đối với từng kết quả. Còn cách thứ 3, thì cache cả 1 func luôn 🙄🙄. Đó chính là điều làm nên sự khác biệt

### Nên sử dụng memoizer khi nào?
Vì memoize cũng là caching, tức là dữ liệu tính toán xong được lưu lại và nằm đó luôn, không mất đi đâu cả, cho nên bạn phải nghĩ tới vấn đề khi app chạy trong một khoản thời gian dài, thì dung lưọng bộ nhớ cũng vì thế mà tăng lên. Cho nên cần phải giải phóng bộ nhớ cache của hàm memoize sau một khoản thời gian nhất định, nếu không sẽ dẫn tới tràn bộ nhớ hoặc memleak.

Vì sử dụng memoize tức là bạn đang đánh đổi memory để lấy tốc độ, chứ không có cái gì là free cả, nên nếu biết chừng mực và dùng đúng lúc đúng chỗ thì sẽ đem lại hiệu quả cao, còn ngược lại, lạm dụng quá mức thì hậu quả sẽ khó lường. Cho nên gì gì thì cũng phải hiểu để xài nhau cho tốt hơn, nhé :D

Còn nếu server của bạn trâu quá rồi. Ví dụ server của mấy thằng *chính phủ* chẳng hạn. Làm cái web vớ vẩn, lượt truy cập thì ít nhưng vẫn quất *AWS tầm vài chục ngàn $Trump 1 tháng* thì chả có gì phải lo cả =)))

Đây là 4 trường hợp trong đó việc *memoization* sẽ có lợi:

1. Đối với các gọi hàm expensive function, tức là các hàm thực hiện các tính toán nặng.
2. Đối với các hàm có phạm vi đầu vào hạn chế và định kỳ cao (highly recurring input range), sao cho các giá trị được lưu trong bộ nhớ cache không chỉ ở im đó mà còn không thay đổi gì cả.
3. Đối với các hàm đệ quy với các giá trị đầu vào định kỳ (recurring input).
4. Đối với các hàm thuần (pure functions), tức là các hàm trả về cùng một đầu ra mỗi lần chúng được gọi với một đầu vào cụ thể.

## Tóm lại

Việc chia nhóm như nào, sử dụng các loại Promise như nào hoặc cấu trúc code như nào là do bạn quyết định. Thông thường sẽ liên quan đến cấu hình server, design pattern của từng dự án hoặc các ý tưởng mà bạn có thể nghĩ ra để tối ưu

Còn 1 số cách tối ưu khác nữa như sử dụng Memoization hoặc Cache tuỳ thuộc vào mức độ cần thiết

```
With programming, the only limit is your imagination.
```

## Nguồn tham khảo

1. [Viblo](https://viblo.asia/p/do-phuc-tap-cua-thuat-toan-EoDkQOdNGbV)
2. [Viblo](https://viblo.asia/p/do-phuc-tap-thuat-toan-anh-huong-cua-o-lon-toi-performance-Ljy5VdjMZra)
3. [Stack Overflow](https://stackoverflow.com/questions/53798589/await-loop-vs-promise-all)
4. [Kipalog](https://kipalog.com/posts/Promise-la-khi-gi-)
5. [Bitsrc](https://blog.bitsrc.io/introduction-to-promise-race-and-promise-any-with-real-life-examples-9d8d1b9f8ec9)
